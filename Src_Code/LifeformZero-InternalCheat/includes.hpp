#pragma once
#include <Windows.h>
#include <fstream>
#include <iostream>
#include <d3d11.h>
#include <dxgi.h>
#include <MinHook.h>
#include "3rdparty_headers/ImGui/imconfig.h"
#include "3rdparty_headers/ImGui/imgui_impl_dx11.h"
#include "3rdparty_headers/ImGui/imgui.h"
#include "3rdparty_headers/ImGui/imgui_internal.h"
#include "3rdparty_headers/ImGui/imstb_rectpack.h"
#include "3rdparty_headers/ImGui/imstb_textedit.h"
#include "3rdparty_headers/ImGui/imstb_truetype.h"
#include "3rdparty_headers/kiero/kiero.h"
#include "3rdparty_headers/ImGui/imgui_impl_win32.h"
#include "3rdparty_headers/nimbussansfont.hpp"
#include "3rdparty_headers/LifeformZero_Features.hpp"
#pragma comment (lib, "d3d11.lib")
#include "3rdparty_headers/console-color.hpp"
using namespace std;
typedef HRESULT(__stdcall* Present) (IDXGISwapChain* pSwapChain, UINT SyncInterval, UINT Flags);
typedef LRESULT(CALLBACK* WNDPROC)(HWND, UINT, WPARAM, LPARAM);
typedef uintptr_t PTR;
Present oPresent;
using namespace std;
// Declare internal and external function for handling Win32 window messages
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
HWND window = NULL;
WNDPROC oWndProc;
ID3D11Device* pDevice = NULL;
ID3D11DeviceContext* pContext = NULL;
ID3D11RenderTargetView* mainRenderTargetView;
bool init = false;
LRESULT __stdcall WndProc(const HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) 
{

	if (true && ImGui_ImplWin32_WndProcHandler(hWnd, uMsg, wParam, lParam))
		return true;

	return CallWindowProc(oWndProc, hWnd, uMsg, wParam, lParam);
}

void InitImgui() {
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	io.ConfigFlags = ImGuiConfigFlags_NoMouseCursorChange;
	io.Fonts->AddFontFromMemoryTTF(nimbussans, sizeof(nimbussans), 18.F);
	ImGui::StyleColorsDark();
	ImGui_ImplWin32_Init(window);
	ImGui_ImplDX11_Init(pDevice, pContext);
}
void EndAndRender() {
	ImGui::End();
	ImGui::Render();
}
HRESULT __stdcall hkPresent(IDXGISwapChain* pSwapChain, UINT SyncInterval, UINT Flags)
{
	if (!init)
	{
		if (SUCCEEDED(pSwapChain->GetDevice(__uuidof(ID3D11Device), (void**)&pDevice)))
		{
			pDevice->GetImmediateContext(&pContext);
			DXGI_SWAP_CHAIN_DESC sd;
			pSwapChain->GetDesc(&sd);
			window = sd.OutputWindow;
			ID3D11Texture2D* pBackBuffer{ 0 };
			pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
			pDevice->CreateRenderTargetView(pBackBuffer, NULL, &mainRenderTargetView); //False Positive Warning DirectX 11 SDK(Idk Why)
			pBackBuffer->Release();
			oWndProc = (WNDPROC)SetWindowLongPtr(window, GWLP_WNDPROC, (LONG_PTR)WndProc);
			InitImgui();
			init = true;
		}

		else
			return oPresent(pSwapChain, SyncInterval, Flags);
	}
	ImGuiWindowFlags fl = ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar;
	ImGui_ImplDX11_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();
	ImGui::Begin("LifeformZero-InternalCheat by RiritoNinigaya", 0, fl);
	ImGui::SetWindowSize(ImVec2(500, 500));
	ImGui::TextColored(ImVec4(55, 75, 215, 255), "This is My First Cheat for Lifeform Zero... \nMade by RiritoNinigaya");
	if (ImGui::BeginMenu("Credit Menu")) 
	{
		int creds = 6000;
		int orig_cred = 0;
		int creditm_add = 500000;
		ImGuiSliderFlags flags_int = ImGuiSliderFlags_NoInput | ImGuiSliderFlags_NoRoundToFormat;
		ImGui::SliderInt("Credits", &creds, orig_cred, creditm_add, "%d", flags_int);
		if (ImGui::Button("Set Credits")) {
			//Features::AddCredit(creds);
			cout << (("Now not Worked!!!"_cs << cs::color::color(0x15702C)));
		}
		if (ImGui::Button("Get FoV")) {
			Features::GetFoV();
		}
		ImGui::EndMenu();
	}
	EndAndRender();
	pContext->OMSetRenderTargets(1, &mainRenderTargetView, NULL);
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
	return oPresent(pSwapChain, SyncInterval, Flags);
}
	