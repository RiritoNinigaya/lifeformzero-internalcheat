#pragma once
#include "Mono_Class.hpp"
#include <iostream>
#include <eh.h>
#include "../MonoHook.h"
#include "UnityResolve/UnityResolve.hpp"
#include "console-color.hpp"
using namespace std;
using namespace cs;
inline void* pObjectAddCredit = nullptr;
void* oObject_addcredit = nullptr;
void* hObject_addcredit = nullptr;
using I = UnityResolve;
using IM = UnityResolve::Method;
using IC = UnityResolve::Class;
using IT = UnityResolve::Type;
using IF = UnityResolve::Field;
using IA = UnityResolve::Assembly;
using II = UnityResolve::UnityType;
namespace Features 
{
	void GetFoV() {
		__try {
			auto CoreModule = II::Camera::GetMain();
			float fov = CoreModule->GetFoV();
			cout << "FoV: " << fov << endl;
			Sleep(4000);
		}
		__except (EXCEPTION_EXECUTE_HANDLER) {
			cout << "Failed To Allocate Memory Camera Function!!!" << endl;
			exit(210);
		}
	}
	void AddCredit(int credit_amount) {
		MonoMethod* AddCredit_Function = Mono::Instance().GetMethod("GeckoGirlDataManager", "AddCredit", 1);
		DWORD ptr_addcredit = (DWORD)AddCredit_Function;
		cout << ptr_addcredit << endl;
		if (AddCredit_Function == nullptr) {
			cout << (("Failed to Allocate AddCredit Function"_cs << cs::color::color(0x15702C)));
		}
		void* args_addcredit[1] = { &credit_amount };
		MonoObject* Object_addcredit = Mono::Instance().Invoke(AddCredit_Function, nullptr, args_addcredit);
		CreateHook(Object_addcredit);
	}
	HOOK_DEF(void, Object_addcredit, (void* __this, int credit))
	{
		// Do stuff

		pObjectAddCredit = __this;
		int amount_credits = 500000;
		credit = amount_credits;
		return oObject_addcredit(__this, credit);
	}
	void SetTimeScale(float flTargetTimeScale)
	{
		MonoMethod* set_timeScale = Mono::Instance().GetMethod("Time", "set_timeScale", 1, "UnityEngine.CoreModule", "UnityEngine");

		void* pArgs[1] = { &flTargetTimeScale };
		MonoObject* pResult = Mono::Instance().Invoke(set_timeScale, nullptr, pArgs);
		cout << std::hex << pResult << std::dec << " | Time Scale set to: " << flTargetTimeScale << endl;
	}
}